import atexit
import os
import time
from datetime import datetime

import pytz
import redis
from apscheduler.schedulers.background import BackgroundScheduler
from flask import Flask, jsonify, render_template, request
from flask_cors import CORS, cross_origin

from AirQualityMonitor import AirQualityMonitor

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
aqm = AirQualityMonitor()


FREQUENCY_SECONDS = int(os.environ.get('FREQUENCY_SECONDS', '120'))

if FREQUENCY_SECONDS > 0:
    # Run the first time at start before the scheduler
    aqm.save_measurement_to_redis()
    scheduler = BackgroundScheduler()
    scheduler.add_job(func=aqm.save_measurement_to_redis, trigger="interval", seconds=FREQUENCY_SECONDS)
    scheduler.start()
    atexit.register(lambda: scheduler.shutdown())

timezone = pytz.timezone('US/Eastern')

def pretty_timestamps(measurement):
	timestamps = []
	for x in measurement:
		timestamps += [datetime.fromtimestamp(x['time']).astimezone(timezone).strftime('%x %I:%M%p')]
	return timestamps

def reconfigure_data(measurement):
    """Reconfigures data for chart.js"""
    current = int(time.time())
    measurement = measurement[:30]
    measurement.reverse()
    return {
        'labels': pretty_timestamps(measurement),
        'aqi': {
            'label': 'Air Quality Index (aqi)',
            'data': [x['measurement']['aqi'] for x in measurement],
            'backgroundColor': '#181d27',
            'borderColor': '#181d27',
            'borderWidth': 3,
        },
        'pm10': {
            'label': 'Inhalable Particles (pm10)',
            'data': [x['measurement']['pm10'] for x in measurement],
            'backgroundColor': '#cc0000',
            'borderColor': '#cc0000',
            'borderWidth': 3,
        },
        'pm2': {
            'label': 'Fine Inhalable Particles (pm2.5)',
            'data': [x['measurement']['pm2.5'] for x in measurement],
            'backgroundColor': '#42C0FB',
            'borderColor': '#42C0FB',
            'borderWidth': 3,
        },
    }

@app.route('/')
def index():
    """Index page for the application"""
    context = {
        'historical': reconfigure_data(aqm.get_last_n_measurements()),
    }
    return render_template('index.html', context=context)


@app.route('/api/')
@cross_origin()

def api():
    """Returns historical data from the sensor"""
    context = {
        'historical': reconfigure_data(aqm.get_last_n_measurements()),
    }
    return jsonify(context)


@app.route('/api/now/')
def api_now():
    """Returns latest data from the sensor"""
    context = {
        'current': aqm.get_measurement(),
    }
    return jsonify(context)


if __name__ == "__main__":
    app.run(debug=True, use_reloader=False, host='0.0.0.0', port=int(os.environ.get('PORT', '8000')))
