# Raspberry Pi Air Quality Monitor

A simple air quality monitoring service for the Raspberry Pi. This was forked from [here](https://github.com/rydercalmdown/pi_air_quality_monitor)

## Installation
From the Raspberry Pi:
```bash
sudo apt update && sudo apt install -y git
cd
git clone https://gitlab.com/archetypalsxe/pi-air-quality-monitor-software.git
cd pi-air-quality-monitor-software
cp env-example env
```

* Running `tofu output` from the other repository will list some values we need to store on the Pi Server
* All of these will be stored in the `pi-air-quality-monitor-software` directory on your Pi
* `iot_endpoint`
  * Copy the `env-example` to `env`
  * Modify this `env` file to set the real value for the endpoint
    * Can also be obtained through this AWS CLI command: `aws iot describe-endpoint --endpoint-type iot:Data-ATS`
    * Should look something like: `UID-ats.iot.us-east-1.amazonaws.com`
      * Where `UID` is a unique 14 digit alpha-numeric AWS account identifier
    * Can also modify the `FREQUENCY_SECONDS` which is how frequently (in seconds) the server will take a reading in the background
* Create the following files from the Terraform output in the `pi-air-quality-monitor-software` directory on your Pi server
  * **Note that we substituted underscores for periods in the output names as OpenTofu won't let you use periods in the names**
  * `ca.pem`
  * `cert.pem.crt`
  * `key.pem.key`
    * This value will be hidden by default
    * Can be revealed with: `tofu output -raw key_pem_key`

## Building the Docker Containers
```bash
make install
make build &
```

### Notes
* `make install` will install some dependencies including Docker and Docker Compose
* `make build` builds the Docker images accordingly and may take some time to complete
  * The installation of `awsiotsdk` takes 43721894739814321 years
    * When I ran it myself it actually took over 10 minutes, but less than 15
  * During this time it kept causing my SSH connection to drop
  * I added the `&` at the end of the command so it could potentially complete even if the SSH connection dropped
  * Another alternative in order to have all logs sent to a file:
    * `make build > /tmp/build-logs.txt 2>&1 &`
  * Changes to the `src` directory, do not require the application to be rebuilt...
    * Just have to exit any running instances and rerun

### Additional Make Commands
**These commands require that the `PI_IP_ADDRESS` and `PI_USERNAME` variables are set in the `Makefile` and are not ran on a Raspberry Pi, but "locally"**
* `make copy`
  * Copies data from the Pi to your local machine
* `make shell`
  * This allows you to SSH into your Pi from locally
  * Recommend setting up an alias in your `/etc/hosts` file and using `~/.ssh/config` for SSH configs, but this is another alternative

## Running
Before proceeding, the SDS011 sensor should be connected to the Pi. The docker compose commands that are running will be forked to the background

To run, use the run command from the `~/pi-air-quality-monitor-software` directory:
```bash
make run
```

## Stopping
This will stop and remove the running docker containers and tear down the networking
```bash
make stop
```

## Testing Registering Device with AWS IoT and Sending Messages
* From within the Docker image and the `/code/` directory:
  * `python3 aws-samples/pubsub.py --cert cert.pem.crt --key key.pem.key --endpoint ${ENDPOINT:?} --ca_file ca.pem`
* If you get an error similar like: `awscrt.exceptions.AwsCrtError: AWS_ERROR_MQTT_UNEXPECTED_HANGUP: The connection was closed unexpectedly.`
  * It may be because the certificate is no longer valid
  * If this is the case, you have to run the OpenTofu again to have a new certificate generated and update the certs accordingly
* By default you won't see the messages come in under the `Activity` tab in the AWS IoT Things console
* To see the messages...
  * In the AWS IoT console
  * Under the `Manage` section, `All devices`, select `Things`
  * You should see an `air_quality_pi`, select it
  * Click on the `Activity` tab, it will be blank
  * Click on the `MQTT test client` button
  * With the `Subscribe to a topic` tab selected...
  * Enter `test/topic` as the topic filter, and click `Subscribe`
  * If you run the `python3` command again, the messages should show up on this page

## Architecture
This project uses python, flask, docker-compose and redis to create a simple web server to display the latest historical values from the sensor.

## Example Data
Some example data you can get from the sensor includes the following:

```json
{
  "device_id": 13358,
  "pm10": 10.8,
  "pm2.5": 4.8,
  "timestamp": "2021-06-16 22:12:13.887717"
}
```

The sensor reads two particulate matter (PM) values.

PM10 is a measure of particles less than 10 micrometers, whereas PM 2.5 is a measurement of finer particles, less than 2.5 micrometers.

Different particles are from different sources, and can be hazardous to different parts of the respiratory system.

## Chart.js
Chart.js is used to create the chart on the web page. Documentation on the available options can be found [here](https://www.chartjs.org/docs/latest/charts/line.html).

## Redis

Redis is used for the database within the Kubernetes cluster

### [Helm Dependency](https://helm.sh/docs/intro/install/)

This should be done outside the cluster, or at least on a computer with `kubectl` access

#### Arch Linux
* `pacman -S helm`

#### Helm's Install Script
```bash
cd /tmp
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
sh get_helm.sh
```

#### Testing It Installed

`helm version`

### [Kustomize Dependency](https://kustomize.io/)

#### [Official Install Docs](https://kubectl.docs.kubernetes.io/installation/kustomize/)

#### Arch Linux
`pacman -S kustomize`

### Homebrew
`brew install kustomize`

### Redis Prerequirements

* From the `k8s/redis-prereqs` directory
* `kubectl diff -k .`
* If everything looks good: `kubectl apply -k .`
* Populate the Redis password secret
  * Doing this temporarily until we get External Secrets added to the project
  * `echo -n "${SECRET_VALUE:?}" | base64`
    * You will need to set `SECRET_VALUE` to a (hopefully) randomly generated and strong password
  * `kubectl edit -n air-quality secret redis-password`
    * Add in the base64 decoded password we generated in the last step:
```
data:
  REDIS_PASSWORD: HASH_FROM_PREVIOUS_STEP
```
  * If done correctly this should return the password you used (decoded):
    * `kubectl get secret "redis-password" -o jsonpath="{.data.REDIS_PASSWORD}" | base64 --decode`

### Exporting/Retrieving Redis Password
* This doc assumes that you have `REDIS_PASSWORD` set as an environment variable
* `export REDIS_PASSWORD=$(kubectl get secret --namespace "air-quality" redis-password -o jsonpath="{.data.REDIS_PASSWORD}" | base64 -d)`

### Redis Helm Chart

This can be done as part of the initial install, or if you want to update anything

* From the `k8s/redis` directory:
* `kustomize build . --enable-helm | kubectl diff -f -`
  * Will show you the changes that are going to be made
* Apply these changes if everything looks good:
  * `kustomize build . --enable-helm | kubectl apply -f -`


### Testing from Kubernetes
* Run a client pod:
```
kubectl run -n air-quality redis-client --rm --tty -i --restart='Never' \
 --env REDIS_PASSWORD=$REDIS_PASSWORD \
 --image redis -- sh
```
* `redis-cli -c -h redis-cluster -a $REDIS_PASSWORD`
  * Connect to Redis

### Testing Redis from a Cluster Node
This can be done from a node in the cluster... currently not exposing the Redis cluster externally

* The service IP can be found by running:
  * `kubectl get svc`
  * It'll be the cluster IP for the `redis-cluster` service
* `docker run --rm -it redis:alpine`
* `docker exec -it ${CONTAINER_ID:?} sh`
* `redis-cli -c -h ${SERVICE_IP} -a ${REDIS_PASSWORD:?}`

### Installing Redis-CLI on the PI
From the Pi, run the following commands:
```bash
sudo apt update
sudo apt install redis-tools
```

### Using Redis-CLI
* Getting number of entries:
  * `LLEN measurements`
* Getting a list of all entries:
  * `lrange measurements 0 -1`
* Getting the last 3 entries:
  * `lrange measurements 0 2`

### Failed Attempts

#### KubeDB
* Wasn't able to get it successfully installed
  * A big part of this may be the licensing... licenses are only good for a month, and you're not able to easily renew it
  * A month went by causing my license to expire, and when I requested a new one, I received an expired license

#### Manually Creating a Cluster
* This required too much manual intervention
* You had to run a script in order to setup the cluster
* Pods were treated like pets and if a pod was replaced it caused issues with the cluster

## Take-Measurements Job

### Access Token

* Create an access token in GitLab for pulling images into Kubernetes
  * This is done from the repository
    * Settings
    * Access Tokens
    * Add new token
  * It only needs `read_registry` scopes
  * Make sure to copy the provided code, you can't view it again otherwise

### Access Token Secret

(TODO: Move this into sealed secrets)

Here we'll add a Kubernetes secret for authenticating to GitLab with the secret
```
kubectl create secret docker-registry gitlab-registry \
  --docker-server=registry.gitlab.com \
  --docker-username=${ACCESS_TOKEN_USERNAME:?} \
  --docker-password=${ACCESS_TOKEN:?} \
  --docker-email=${GITLAB_EMAIL:?}
```

### Labeling Nodes with Sensors Connected

We only want the cron job to run on nodes which have a sensor connected to them, therefore we'll label any nodes which have sensors connected accordingly

* `kubectl label nodes ${NODE_NAME:?} air-quality-sensor=1`
  * For each node with a sensor attached
* `kubectl get nodes --show-labels`
  * Should show the label on the nodes with the sensors

### Manually Running CronJob

* `kubectl create job --from=cronjob/take-measurement manual-run-1`